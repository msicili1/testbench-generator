#!/usr/bin/env python3

import os
import sys

#Globals
FILENAME = "system_tb"
TIME = "#10;\n"
COMPTEST = True     #indicates whether the tests in the testbench will cover all possible 
                    #combinations of binary values, or only those specified when running the script
INPUTLIST = []      #will contain the inputs to the testbench
OUTPUTLIST = []     #outputs from the test bench
SYSTEMINPUTS = []   #will contain the inputs to the system module or the unit otherwise under test
SYSTEMOUTPUTS = []  #will contain the outputs from the same unit
TESTLIST = []       #if comprehensive test is not used, will contain the values to be used in each
                    #test

#Functions
def usage(status=0):
    #display usage information and exit with specified status
    print('''Usage: {} -i [inputs] -o [outputs] -si [system inputs] -so [system outputs] [options]

    -i  [largestbitnumber:smallestbitnumber]input    Testbench inputs (example: [3:0]testA)
                                                                      (default: if size is not specified
                                                                       it will be set to one bit)
    -o  [largestbitnumber:smallestbitnumber]output   Testbench outputs (example: [6:0]H0)
    -si System Input Names                           System inputs
    -so System Output Names                          System outputs
    -f  FILENAME                                     Desired testbench file name
    -t  TIME                                         Length of each individual test
    -v  Binary Test Values                           The values to be used for each test case
                                                    (note: number of values must be a multiple of the
                                                     number of inputs, or each input will not have a
                                                     value for every test)
    '''.format(os.path.basename(sys.argv[0])))
    sys.exit(status)

def io(fileName, inputs, outputs):
    #generate the I/O of the testbench
    for input in inputs:
        fileName.write("\treg " + input + ";\n")
    for output in outputs:
        fileName.write("\twire " + output + ";\n")

    fileName.write("\n")

def uut(fileName, inputList, outputList, systemInputs, systemOutputs, testList):
    #generate the unit under test portion of the testbench
    listLoc = 0
    numBitsList = []

    for item in inputList:  #finds the size of each input in bits, then strips the characters 
                            #indicating this, changing the list to contain just the input name
        letterLoc = 0
        if item[0] != '[':  #executes if the size is 1 bit
            numBitsList.append('1')
            continue
        else:
            for letter in item:
                if letter != ']':
                    letterLoc += 1
                else:       #locates the character indicating the size in bits
                    numBits = item[:letterLoc]
                    numBits = int(item[1]) + 1
                    numBitsList.append(numBits)
                    letterLoc += 1
                    item = item[letterLoc:]
                    inputList[listLoc] = item #replaces the input name and size with just the name
                    listLoc += 1

    listLoc = 0
    for item in outputList: #strips the characters indicating the size of the output and changes
                            #the output list to contain only the names of the outputs
        letterLoc = 0
        if item[0] != '[':
            continue
        else:
            for letter in item:
                if letter != ']':
                    letterLoc += 1
                else:
                    letterLoc += 1
                    item = item[letterLoc:]
                    outputList[listLoc] = item
                    listLoc += 1

    fileName.write("\t")
    for letter in FILENAME: #determines the name of the unit under test from the name of the
                            #testbench file
        if letter != '_':
            fileName.write(letter)
        else:
            break
    fileName.write(" uut(\n")

    #prints each of the corresponding system and tesbench inputs and outputs for the uut portion of
    #the testbench
    for number in range(len(systemInputs)):
        fileName.write("\t\t." + systemInputs[number] + " (" + inputList[number] + "),\n")
    for number in range(len(systemOutputs)):
        if max(range(len(systemOutputs))) - number > 0:
            fileName.write("\t\t." + systemOutputs[number] + " (" + outputList[number] + "),\n")
        else: #makes sure that the last line of the unit under test doesn't end in a comma
            fileName.write("\t\t." + systemOutputs[number] + " (" + outputList[number] + ")\n")
    fileName.write("\t);\n\n")

    if COMPTEST == True:
        compTest(fileName, inputList, numBitsList)
    else:
        Test(fileName, inputList, testList, numBitsList)

def Test(fileName, inputList, testList, numBitsList):
    #generate the sets of values to test from the values specified by the -v command
    fileName.write("\tinitial begin\n")

    for test in testList:
        inputLoc = 0
        fileName.write("\t\t")
        for value in test:
            fileName.write(inputList[inputLoc] + " = " + str(numBitsList[inputLoc])+ "'b")
            fileName.write(value + "; ")
            inputLoc += 1
        fileName.write("\n\t\t#10;\n")

    fileName.write("\n\t$stop;\n\tend\nendmodule\n")

def compTest(fileName, inputs, numBitsList):
    #generates every possible test given the sizes of the inputs
    fileName.write("\tinitial begin\n\t\t")
    inputDict = {}
    inputLoc = 0

    for item in inputs: #creates a nested dictionary containing information about each input
        inputSubDict = {}
        inputSubDict["length"] = numBitsList[inputLoc]
        inputSubDict["max"] = (2**numBitsList[inputLoc]) - 1
        inputSubDict["value"] = 0
        inputDict[item] = inputSubDict
        inputLoc += 1
    
    #think of the following code as a series of counters. The rightmost counter counts upwards from
    #zero. When it reaches its maximum value, the counter to its left increments. This goes on until
    #the leftmost counter has reached its maximum value, meaning that each possible combination
    #of values has been covered.
    while inputDict[inputs[0]]["value"] <= inputDict[inputs[0]]["max"]: #while every possible combo
                                                                        #has not been covered
        incLoc = -1
        finishedFlag = False

        while inputDict[inputs[incLoc]]["value"] == inputDict[inputs[incLoc]]["max"]:
        #executes when a counter reaches its maximum. Will go through each counter until it finds
        #one that has not reached its maximum.
            if inputs[incLoc] == inputs[0]: #if the counter at its maximum being checked is the
                                            #leftmost one, the test generation is finished
                finishedFlag = True
                break
            inputDict[inputs[incLoc]]["value"] = 0 #set the current counter to zero
            incLoc -= 1                            #and check the next one
        if finishedFlag == True:
            break

        inputDict[inputs[incLoc]]["value"] += 1 #increment the counter found to not be at its max
        inputLoc = 0
        for item in inputs:
            #generate the binary value to be assigned to the input (e.g. 3'b010)
            inputDict[inputs[inputLoc]]["binVal"] = str(inputDict[inputs[inputLoc]]["length"])
            inputDict[inputs[inputLoc]]["binVal"] += "'b"
            binaryValue = bin(inputDict[inputs[inputLoc]]["value"])[2:] 
            #finds the binary equivalent of the value expressed in as few digits as possible

            while len(binaryValue) < int(inputDict[inputs[inputLoc]]["length"]):
            #appends zeros to the front of the binary value until its size is equivalent to the
            #number of bits of its corresponding input
                binaryValue = "0" + binaryValue

            inputDict[inputs[inputLoc]]["binVal"] += binaryValue
            fileName.write(item + " = " + inputDict[inputs[inputLoc]]["binVal"] + "; ")
            inputLoc += 1

        fileName.write("\n\t\t#10;\n\t\t")
    fileName.write("\n\t$stop;\n\tend\nendmodule\n")

#Command line options
args = sys.argv[1:]

if not len(args):
    usage()

while len(args) and args[0].startswith('-'):
    arg = args.pop(0)
    if (arg == "-f"):       #set filename
        arg = args.pop(0)
        FILENAME = arg
    if (arg == "-t"):       #set test length
        arg = args.pop(0)
        TIME = "#" + arg + ";\n"
    if (arg == "-i"):       #create the list of inputs
        while len(args) and not args[0].startswith('-'):
            arg = args.pop(0)
            INPUTLIST.append(arg)
    elif (arg == "-o"):     #create the list of outputs
        while len(args) and not args[0].startswith('-'):
            arg = args.pop(0)
            OUTPUTLIST.append(arg)
    elif (arg == "-si"):    #create the list of system inputs
        while len(args) and not args[0].startswith('-'):
            arg = args.pop(0)
            SYSTEMINPUTS.append(arg)
    elif (arg == "-so"):    #create the list of system outputs
        SYSTEMOUTPUTS
        while len(args) and not args[0].startswith('-'):
            arg = args.pop(0)
            SYSTEMOUTPUTS.append(arg)
    elif (arg == "-v"):     #create the list of test values
        if not len(INPUTLIST):
            raise RuntimeError("Inputs must be specified with the -i flag before values can be \
                                passed with the -v flag.")
        
        valsPerTest = len(INPUTLIST)
        COMPTEST = False

        while len(args) and not args[0].startswith('-'): #creates a list containing lists containing
                                                         #the values to be tested in each test
            test = []
            while len(test) < valsPerTest:
                arg = args.pop(0)
                test.append(arg)
            TESTLIST.append(test)

#Main
outputFile = open(FILENAME + ".v", "w")
outputFile.write("`timescale 1ns/1ns\n\nmodule " + FILENAME + "();\n")

io(outputFile, INPUTLIST, OUTPUTLIST)
uut(outputFile, INPUTLIST, OUTPUTLIST, SYSTEMINPUTS, SYSTEMOUTPUTS, TESTLIST)
